// Task 1: Back to front string
function backToFront(str, count) {
    if (count >= str.length) {
      return str;
    } else {
      const front = str.slice(-count);
      const back = str.slice(0, -count);
      return front + back + str;
    }
  }
  
  console.log(backToFront('hello', 1)); // Output: ohelloo
  console.log(backToFront('abc', 3)); // Output: abcabcabc
  console.log(backToFront('world', 2)); // Output: ldworldld
  console.log(backToFront('world', 20)); // Output: world
  
  // Task 2: Nearest number
  function nearest(x, y, z) {
    const diff1 = Math.abs(x - z);
    const diff2 = Math.abs(y - z);
  
    if (diff1 < diff2) {
      return x;
    } else {
      return y;
    }
  }
  
  console.log(nearest(100, 22, 122)); // Output: 122
  console.log(nearest(50, 22, 122)); // Output: 22
  
  // Task 3: Remove array duplicates
  function removeDuplicate(arr) {
    const result = [];
    for (let i = 0; i < arr.length; i++) {
      if (!result.includes(arr[i])) {
        result.push(arr[i]);
      }
    }
    return result;
  }
  
  console.log(removeDuplicate([1, 2, 3, 2, 3, 1, 1])); // Output: [1, 2, 3]
  console.log(removeDuplicate(['a', 1, '2', 'b', 1, '2', 'b'])); // Output: ['a', 1, '2', 'b']